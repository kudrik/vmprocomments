<?php

namespace MovingImage\Bundle\VMProComments\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use MovingImage\Bundle\VMProComments\Entity\Comment;
use MovingImage\Bundle\VMProComments\Form\CommentForm;


class DefaultController extends Controller
{
    public function indexAction()
    {
        return new \Symfony\Component\HttpFoundation\Response('Comments area');
    }

    public function addAction(string $videoId, Request $request)
    {
        $form = $this->createForm(CommentForm::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $comment = $form->getData();
            $comment->setVideoId($videoId);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            //@TODO return json respons
            return new \Symfony\Component\HttpFoundation\Response('comment add');
        }


    }

    public function listAction(string $videoId)
    {
        $comments = $this->getDoctrine()->getRepository(Comment::class)->findAllByVideoId($videoId);

        //@TODO return json api
        return new \Symfony\Component\HttpFoundation\Response(print_R($comments,1));
    }
}
