<?php

namespace MovingImage\Bundle\VMProComments\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CommentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Content', array('class' => 'col-md-6'))
            ->add('name', 'text')
            ->end()
            ->with('Description', array('class' => 'col-md-12'))
            ->add('description', 'textarea', array('required' => false, 'attr' => array('class' => 'mceAdvanced')))
            ->end()
            ->with('Advansed', array('class' => 'col-md-6'))
            ->add('forfree', 'checkbox', array('required' => false))
            ->add('position', 'integer', array('required' => false))
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('user_name');
        $datagridMapper->add('user_email');
        $datagridMapper->add('text');
        $datagridMapper->add('video_id');
        $datagridMapper->add('status');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper->addIdentifier('date');
        $listMapper->addIdentifier('user_name');
        $listMapper->addIdentifier('user_email');
        $listMapper->addIdentifier('text');
        $listMapper->addIdentifier('videoId');
        $listMapper->add('status', 'boolean', ['editable' => true ]);
        $listMapper->add('_action', 'actions', array(
            'actions' => array(
                'delete' => array(),
            )
        ));

    }
}