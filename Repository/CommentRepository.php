<?php

namespace MovingImage\Bundle\VMProComments\Repository;

use Doctrine\ORM\EntityRepository;

class CommentRepository extends EntityRepository
{
    /**
     * @param string $video_id
     * @return array
     */
    public function findAllByVideoId(string $video_id) : array
    {
        return $this->findBy(array(
            'video_id' => $video_id,
            'status' => 1,
        ));
    }
}